from ast import literal_eval
import pandas as pd
import os.path

import torch
from sentence_transformers import SentenceTransformer, util
from transformers import GPT2Tokenizer, GPT2LMHeadModel, AutoModel
from constants import ARGS_DATASET

from gpt2 import generate_sentence_gpt2
import bert_train
from doc2vec_training import get_pandas_arguments

def preprocess_sentence(sentence):
    processed_sentence = sentence
    if len(processed_sentence) <= 3:
        return
    for word in processed_sentence.split():
        if "http://" in word or "https://" in word:
            return
    return processed_sentence

#load sbertModel
def load_Model():
  # see https://www.sbert.net/docs/pretrained_models.html for avail models
  print('Loading bert model...')
  model = 'all-MiniLM-L6-v2'
  sTransfomerModel = SentenceTransformer(model)
  #biggest model
  #model = 'all-MiniLM-L12-v2'
  # To load bert model from local source, see https://discuss.huggingface.co/t/download-models-for-local-loading/1963
  #sTransfomerModel = AutoModel.from_pretrained('./all-MiniLM-L6-v2')
  #set seq.length
  sTransfomerModel.max_seq_length = 256
  return sTransfomerModel

#seach function
def search(query, corpus_embeddings, top_k, sTransfomerModel, documents):
  #print('Input question: ', query)
  #results sbert
  question_embedding = sTransfomerModel.encode(query, convert_to_tensor=True),
  # calc cosine similarity
  hits = util.semantic_search(question_embedding[0], corpus_embeddings, top_k=top_k)
  hits = hits[0]

  #Output of top-5 hit
  #print("Top-5 Bert Retrieval hits")
  hits = sorted(hits, key=lambda x: x['score'], reverse=True)
  # for hit in hits[0:5]:
  #   print("\t{:.3f}\t{}".format(hit['score'], documents[hit['corpus_id']].replace("\n", " ")))

  return hits

  # best_5 = []
  # for corp in hits[0:5]:
  #   best_5.append(documents[corp['corpus_id']])
  # return best_5

if __name__ == '__main__':
  PREPROCESS = True
  fileName = 'bertEmbedding'

  print('Loading dataset...')
  nrows = 1000
  chunksize = 10000
  documents = []
  
  #df = pd.read_csv(ARGS_DATASET, nrows=nrows)
  for chunk in pd.read_csv(ARGS_DATASET, chunksize=chunksize, nrows=nrows):
    for index, doc, st in get_pandas_arguments(chunk):
      if PREPROCESS:
        if preprocess_sentence(doc) is not None:
          documents.append(preprocess_sentence(doc))
      elif not PREPROCESS:
        documents.append(doc)
  print('finished loading dataset')

  bertModel = load_Model()

  if not os.path.exists(fileName):
    bert_train.train_Bert(documents, fileName, bertModel)
    corpus_embeddings = torch.load(fileName)
  else:
    print('loading sbert embeddings')
    corpus_embeddings = torch.load(fileName)

  #Retrieve top k matching entries.
  top_k = 100

  # example query
  bert_results = search(query='War', corpus_embeddings=corpus_embeddings, top_k=top_k, sTransfomerModel=bertModel, documents=documents)
  best_5 = []
  for hit in bert_results[0:5]:
            print("\t{:.3f}\t{}".format(hit['score'], documents[hit['corpus_id']].replace("\n", " ")))
            best_5.append(documents[hit['corpus_id']])

  #get 5best fitting sentences for query
  print('gpt2: ')
  #gpt2
  tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
  model_gpt = GPT2LMHeadModel.from_pretrained('gpt2', pad_token_id=tokenizer.eos_token_id)
  print(generate_sentence_gpt2(best_5[0], tokenizer, model_gpt))