import argparse
from email.utils import formatdate
import os
from pyexpat import model
from time import sleep
from xml.dom.xmlbuilder import DocumentLS
import xml.etree.ElementTree as Et

import pandas as pd
import numpy as np
import torch
from elasticsearch import Elasticsearch
from gensim.models import Doc2Vec
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import openai


from doc2vec_training import get_pandas_arguments

from constants import ELASTIC_HOST_NAME, ELASTIC_INDEX_NAME, ARGS_DATASET
from doc2vec_sentence_pairs import get_doc2vec_sentence_pairs
from bert import load_Model, search, preprocess_sentence
from query import perform_query
from gpt2 import generate_a_lot_sentences_gpt2
from gpt3 import gpt3_text_generation


def parse_topics(topics_file):
    tree = Et.parse(topics_file)
    root = tree.getroot()
    topics = []
    for child in root:
        d = {'topic_number': int(child[0].text), 'topic_query': child[1].text}
        topics.append(d)
    topics = pd.DataFrame(topics)
    return topics


if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-dir')
    parser.add_argument('-o', '--output-dir')
    args = parser.parse_args()
    args = vars(args)

    # wait to give the containers time to start
    # sleep(60)

    # connect to elasticsearch node
    es = Elasticsearch(hosts=ELASTIC_HOST_NAME, timeout=300)
    while not es.ping():
        sleep(10)
        print('Waiting for elasticsearch node...')
        es = Elasticsearch(hosts=ELASTIC_HOST_NAME, timeout=300)

    # sanity check
    print(es.ping())
    print(es.indices.get_mapping(index=ELASTIC_INDEX_NAME))

    # handle input
    input_dir = args['input_dir']
    output_dir = args['output_dir']

    # make sure the output directory exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # parse topics
    print('Parsing topics...')
    topics = parse_topics(os.path.join(input_dir, 'topics.xml'))

    # Loading bert model
    bertModel = load_Model()
    bert_model_filename = './Small_Embedding_all-MiniLM-L6-v2.pt'
    if os.path.exists(bert_model_filename):
        print('Using previously trained bert model.')
        bert_corpus_embedding = torch.load(bert_model_filename)
    else:
        print('Bert model not available.')
        bert_corpus_embedding = None

    # initialise gpt models and tokenizer
    tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    model_gpt = GPT2LMHeadModel.from_pretrained('gpt2', pad_token_id=tokenizer.eos_token_id)

    print('Loading dataset...')
    nrows = 10000
    chunksize = 10000
    documents = []
    PREPROCESS = True

    for chunk in pd.read_csv(ARGS_DATASET, chunksize=chunksize):
        for index, doc, st in get_pandas_arguments(chunk):
            if PREPROCESS:
                if preprocess_sentence(doc) is not None:
                    documents.append(preprocess_sentence(doc))
            elif not PREPROCESS:
                documents.append(doc)
    print('finished loading dataset')

    # execute searches
    for _, row in topics.iterrows():
        topic_number = row['topic_number']
        query = row['topic_query']

        print(f'Now working on query {topic_number}: {query}')
        results = perform_query(query)
        formatted_results = []
        counter = 1
        for result in sorted(results['hits']['hits'], key=lambda k: k['_score']):
            #sentence_pairs_doc2vec = get_doc2vec_sentence_pairs(result, model, tokenizer, model_gpt)
            generated_sentence = generate_a_lot_sentences_gpt2(result['_source']['conclusion'], tokenizer, model_gpt,
                                                               bertModel)
            print("GPT-2 sentence: " + generated_sentence)
            # use generated sentence to retrieve most similar sentences
            bert_results_generated = search(generated_sentence, corpus_embeddings=bert_corpus_embedding, top_k=100,
                                            sTransfomerModel=bertModel, documents=documents)

            for hit in bert_results_generated[0:5]:
                formatted_results.append({
                    'qid': topic_number,
                    'Q0': 'Q0',
                    'pair': f'{result["_source"]["conclusion"]}, {hit["corpus_id"]}',
                    'score': result['_score'] + hit['score'],
                    'tag': 'korg9000'
                })

            final_ranks = pd.DataFrame(formatted_results)
            final_ranks = final_ranks.sort_values(by='score', ascending=False)
            final_ranks['rank'] = np.arange(len(final_ranks)) + 1

            # write results to output file
            with open(f'{output_dir}/run.txt', 'a+') as output_file:
                final_ranks[['qid', 'Q0', 'pair', 'rank', 'score', 'tag']]\
                    .to_csv(output_file, sep=' ', header=False, index=False)
            print(final_ranks)
        break
