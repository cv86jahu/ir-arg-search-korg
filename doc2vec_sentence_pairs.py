from scipy import spatial

from doc2vec_training import preprocess_sentence
from gpt2 import generate_sentence_gpt2


def get_doc2vec_sentence_pairs(elastic_result, model, tokenizer=None, model_gpt=None):
    conclusion = elastic_result["_source"]["conclusion"]
    # since the result is not returned as a string anymore, literal_eval is not needed anymore
    #premises = ast.literal_eval(elastic_result["_source"]["premises"])
    premises = elastic_result["_source"]["premises"]
    arg_stance = premises[0]["stance"]
    #print("Search result: ", elastic_result["_score"], elastic_result["_id"], arg_stance, conclusion)
    inferred_vec_conclusion = model.infer_vector(conclusion.split(), epochs=10)

    ### refined retrieval: look for similar sentences within argument and compare with conclusion vector
    argument_sentences = elastic_result['_source']['sentences']
    sentence_matches = []

    for sen1 in argument_sentences:
        if preprocess_sentence(sen1['sent_text']) is not None:
            vec1 = model.infer_vector(sen1['sent_text'].split(), epochs=10)
            if tokenizer != None:
                generated_sentence = generate_sentence_gpt2(sen1['sent_text'], tokenizer, model_gpt)
                vec_gen = model.infer_vector(generated_sentence.split(), epochs=10)

            for sen2 in argument_sentences:
                if preprocess_sentence(sen2['sent_text']) is not None:
                    if sen1['sent_text'] != sen2['sent_text']:
                        vec2 = model.infer_vector(sen2['sent_text'].split(), epochs=10)

                        # calculate distance between the two sentences
                        cos_distance = spatial.distance.cosine(vec1, vec2)
                        # calculate distance for both sentences between sentence and conclusion
                        cos_distance_conclusion = (spatial.distance.cosine(inferred_vec_conclusion, vec1) + spatial.distance.cosine(inferred_vec_conclusion, vec2)) / 2
                        cos_distance_average = (cos_distance + cos_distance_conclusion) / 2
                        if tokenizer != None:
                            # calculate distance for generated sentence
                            cos_distance_generated = spatial.distance.cosine(vec2, vec_gen)
                            struct1 = [sen1['sent_id'], sen2['sent_id'], sen1['sent_text'], sen2['sent_text'],  cos_distance, cos_distance_conclusion, cos_distance_average, cos_distance_generated]
                            struct2 = [sen2['sent_id'], sen1['sent_id'], sen2['sent_text'], sen1['sent_text'], cos_distance, cos_distance_conclusion, cos_distance_average, cos_distance_generated]
                        else:
                            struct1 = [sen1['sent_id'], sen2['sent_id'], sen1['sent_text'], sen2['sent_text'],  cos_distance, cos_distance_conclusion, cos_distance_average]
                            struct2 = [sen2['sent_id'], sen1['sent_id'], sen2['sent_text'], sen1['sent_text'], cos_distance, cos_distance_conclusion, cos_distance_average]
                        if struct1 not in sentence_matches and struct2 not in sentence_matches:
                            sentence_matches.append(struct1)
    if tokenizer != None:
        # sort by cos_distance_generated
        sentence_matches.sort(key=lambda el: el[7])
        #for match in sentence_matches[0:3]: print(match)
        best_generated = sentence_matches[0:5]

    # sort by cos_distance_average
    sentence_matches.sort(key=lambda el: el[6])
    #for match in sentence_matches[0:3]: print(match)
    best_averaged = sentence_matches[0:5]

    return best_averaged[0]
