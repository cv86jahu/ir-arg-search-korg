import string
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer


lemmatizer = WordNetLemmatizer()


def import_stopwords(path):
    stop_words = []
    with open(path) as file:
        for line in file:
            stop_words.append(line.replace("\n", ""))
    return stop_words


def lemmatize_sentence(sentence):
    token_words = word_tokenize(sentence)
    lemma_sentence = []
    for word in token_words:
        lemma_sentence.append(lemmatizer.lemmatize(word))
        lemma_sentence.append(" ")
    return "".join(lemma_sentence)


def remove_punctation(sentence):
    sentence = sentence.translate(str.maketrans('', '', string.punctuation))
    return sentence


stop_words = import_stopwords('resources/stopwords_en.txt')


def remove_stopwords(tokens):
    query_filtered = [w for w in tokens if not w.lower() in stop_words]
    return query_filtered


def get_synonyms(query):
    synonyms = []
    count = 0
    for x in query:
        for syn in wordnet.synsets(x):
            for l in syn.lemmas():
                if count < 3:
                    count += 1
                    if l.name() not in synonyms:
                        synonyms.append(l.name())
    return synonyms


def get_syns_for_each_token(query):
    synonyms = []
    for token in query:
        if len(wordnet.synsets(token)) != 0:
            for syns in wordnet.synsets(token)[0].lemmas():
                if syns.name() not in query and syns.name() not in synonyms:
                    synonyms.append(syns.name())
                    break
    return synonyms


def query_preprocessing(query):
    query = lemmatize_sentence(query)
    query = remove_punctation(query)
    query = word_tokenize(query)
    query = remove_stopwords(query)
    return query


# Expand query with first three synonyms
def expand_query_v1(query):
    query = query_preprocessing(query)
    synonyms = get_synonyms(query)

    for syn in synonyms:
        if syn not in query:
            query.append(syn)
    return " ".join(query)


# Expand query with first synonym for each token in query
def expand_query_v2(query):
    query = query_preprocessing(query)
    synonyms = get_syns_for_each_token(query)

    for syn in synonyms:
        if syn not in query:
            query.append(syn)
    return " ".join(query)


