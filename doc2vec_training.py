import logging
import os.path
import pandas as pd
import ast

from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.parsing.preprocessing import remove_stopword_tokens, stem
from gensim.utils import simple_tokenize
from gensim import similarities

from constants import DOC2VEC_MODEL, ARGS_DATASET


def get_pandas_arguments(args):
    counter = 1
    for chunk in args:
        print(f'Processing {counter}. chunk...')
        counter += 1
        for sentences, premises in zip(chunk['sentences'], chunk['premises']):
            #sentences = ast.literal_eval(sentences)
            #premises = ast.literal_eval(premises)
            for sentence in sentences:
                yield sentence['sent_id'], sentence['sent_text'], premises[0]['stance']

def preprocess_sentence(sentence):
    if type(sentence) is not list:
        processed_sentence = sentence.split()
    else:
        processed_sentence = sentence
    if len(processed_sentence) <= 3:
        return
    for word in processed_sentence:
        if "http://" in word or "https://" in word:
            return
    return processed_sentence


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    PREPROCESS = True

    filename = DOC2VEC_MODEL

    # check if pretrained model is already available
    if not os.path.exists(filename):
        print('Importing data for training...')
        args = pd.read_csv(ARGS_DATASET, sep=',', chunksize=25000,
                          converters={'premises': ast.literal_eval, 'sentences': ast.literal_eval})
        documents = []
        print('Create documents...')
        for index, doc, st in get_pandas_arguments(args):
            if PREPROCESS:
                if preprocess_sentence(doc) is not None:
                    documents.append(TaggedDocument(preprocess_sentence(doc), [index]))
            elif not PREPROCESS:
                documents.append(TaggedDocument(doc), [index])

        print('Training model...')
        # TODO: experiment with varying vector_size, dm_concat etc.
        # see https://radimrehurek.com/gensim/models/doc2vec.html
        model = Doc2Vec(documents, window=3, min_count=2, workers=4, epochs=10)

        # manually specify model building
        # model.build_vocab(documents)
        # model.train(documents, total_examples=model.corpus_count, epochs=50)
        print('Finished training.')

        # persist to disk
        model.save(filename)
    else:
        print('Model already trained.')


