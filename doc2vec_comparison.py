import argparse
import os
from time import sleep
import xml.etree.ElementTree as Et

import pandas as pd
import numpy as np
from elasticsearch import Elasticsearch
from gensim.models import Doc2Vec
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import openai

from constants import ELASTIC_HOST_NAME, ELASTIC_INDEX_NAME, OPENAI_API_KEY
from doc2vec_sentence_pairs import get_doc2vec_sentence_pairs
from query import perform_query


def parse_topics(topics_file):
    tree = Et.parse(topics_file)
    root = tree.getroot()
    topics = []
    for child in root:
        d = {'topic_number': int(child[0].text), 'topic_query': child[1].text}
        topics.append(d)
    topics = pd.DataFrame(topics)
    return topics


if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-dir')
    parser.add_argument('-o', '--output-dir')
    args = parser.parse_args()
    args = vars(args)

    # wait to give the containers time to start
    # sleep(60)

    # connect to elasticsearch node
    es = Elasticsearch(hosts=ELASTIC_HOST_NAME, timeout=300)
    while not es.ping():
        sleep(10)
        print('Waiting for elasticsearch node...')
        es = Elasticsearch(hosts=ELASTIC_HOST_NAME, timeout=300)

    # sanity check
    print(es.ping())
    print(es.indices.get_mapping(index=ELASTIC_INDEX_NAME))

    # handle input
    input_dir = args['input_dir']
    output_dir = args['output_dir']

    # make sure the output directory exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # parse topics
    print('Parsing topics...')
    topics = parse_topics(os.path.join(input_dir, 'topics.xml'))

    # load doc2vec model
    model_filename = "./args_doc2vec_full_with_prepr.model"
    if os.path.exists(model_filename):
        print('Using previously trained doc2vec model.')
        model = Doc2Vec.load(model_filename)
    else:
        print('Doc2vec model not available.')
        # train model?
        model = None

    tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    model_gpt = GPT2LMHeadModel.from_pretrained('gpt2', pad_token_id=tokenizer.eos_token_id)

    # execute searches
    for _, row in topics.iterrows():
        topic_number = row['topic_number']
        query = row['topic_query']

        print(f'Now working on query {topic_number}: {query}')
        results = perform_query(query)
        formatted_results = []
        counter = 1
        for result in sorted(results['hits']['hits'], key=lambda k: k['_score']):
            sentence_pairs_doc2vec = get_doc2vec_sentence_pairs(result, model, tokenizer, model_gpt)
            print('averaged score sentences:')
            for s in sentence_pairs_doc2vec[0]: print(s)
            print('generated score sentences:')
            for s in sentence_pairs_doc2vec[1]: print(s)

