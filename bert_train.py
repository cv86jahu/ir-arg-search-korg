import pandas as pd
import os.path
import ast

import torch

from doc2vec_training import get_pandas_arguments
import bert
from constants import ARGS_DATASET

def train_Bert(documents, fileName, sTransfomerModel): 
  print('Training sbert model...')
  #remove duplicates?
  corpus_embeddings = sTransfomerModel.encode(list(set(documents)), convert_to_tensor=True, show_progress_bar=True)
  print('Finished training ... :)')
  
  #saving tensor
  print('saving bert embedding')
  torch.save(corpus_embeddings, fileName)

if __name__ == '__main__':
  PREPROCESS = True
  fileName = 'bertEmbedding.pt'

  print('Loading dataset...')
  nrows = 1000
  chunksize = 10000
  documents = []
  number_rows = 100
  # df = pd.read_csv(ARGS_DATASET, nrows=nrows)
  args = pd.read_csv(ARGS_DATASET, sep=',', chunksize=25000, skiprows=lambda i: i % number_rows != 0,
                          converters={'premises': ast.literal_eval, 'sentences': ast.literal_eval})
  for index, doc, st in get_pandas_arguments(args):
    if PREPROCESS:
      if bert.preprocess_sentence(doc) is not None:
        documents.append(bert.preprocess_sentence(doc))
    elif not PREPROCESS:
      documents.append(doc)
  print('finished loading dataset')

  if not os.path.exists(fileName):
    bertModel = bert.load_Model()
    train_Bert(documents, fileName, bertModel)
  else:
    print('No need to train. there are already saved bertembeddings')