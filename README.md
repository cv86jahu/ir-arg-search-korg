# ir-arg-search-korg

Task 1 for Advanced Informatik Retrieval: Building an Argument Search Engine for the Web

### How to
#### Dataset
* [Download](https://files.webis.de/data-in-progress/data-research/arguana/touche/touche22/)

#### Setup Backend
* Install [Docker](https://docs.docker.com/compose/install/)
* Restart system
* Go to project root
* Start container with `docker-compose up`

