import pandas as pd
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import openai
import torch
from sentence_transformers import SentenceTransformer, util
# import nltk
import operator

# nltk.download('punkt')
# from nltk import sent_tokenize
# from nltk.tokenize import word_tokenize, PunktSentenceTokenizer
import os


# greedy search - selects the word with the highest probability as its next word
def greedy_search(inputs, tokenizer, model):
    output = model.generate(inputs, max_length=100)
    return tokenizer.decode(output[0], skip_special_tokens=True)


# num_beams - Beam Search is a heuristic that keeps a set number of candidate sequences until they all
# reach the desired length and keeps the most likely candidate. Search for the output sequence that actually
# maximizes likelihood is intractable, so that beam search provides a reasonable alternative in practice.
def beam_search(inputs, tokenizer, model):
    output = model.generate(inputs, max_length=100, num_beams=10, early_stopping=True)
    return tokenizer.decode(output[0], skip_special_tokens=True)


# temperature - scaling reshapes the output distribution by skewing it either toward high probability tokens
# or low probability tokens: the former improves generation quality but hurts diversity, while the latter
# regularizes generation by making the model less certain of its top choices.
def temperature_search(inputs, tokenizer, model):
    output = model.generate(inputs, do_sample=True, max_length=75, temperature=1.4)
    return tokenizer.decode(output[0], skip_special_tokens=True)


# top_k - t truncates the language model’s output distribution to a set of size k of most likely tokens.
# The sampling then uses relative probability within this truncated set. All other tokens are not considered
# for generation.
def top_k_sampling(inputs, tokenizer, model):
    output = model.generate(inputs, do_sample=True, max_length=75, top_k=75)
    return tokenizer.decode(output[0], skip_special_tokens=True)


# top_p - nucleus sampling ( it's top-k sampling, except instead of k tokens, it takes all tokens until
# probability of these tokens combined is > some parameter p)
def top_p_sampling(inputs, tokenizer, model):
    output = model.generate(inputs, do_sample=True, max_length=75, top_p=0.6)
    return tokenizer.decode(output[0], skip_special_tokens=True)


# general method to access gtp2
def generate_sentence_gpt2(sentence, tokenizer, model, sampling, bert_model):
    inputs = tokenizer.encode(sentence, return_tensors='pt')

    offensive_words = pd.read_csv('./google_profanity_word_list.txt', header=None)

    if sampling == 1:
        generated_sentence = top_k_sampling(inputs, tokenizer, model)
    elif sampling == 2:
        generated_sentence = top_p_sampling(inputs, tokenizer, model)
    elif sampling == 3:
        generated_sentence = temperature_search(inputs, tokenizer, model)

    for word in generated_sentence.split():
        if (word in list(offensive_words[0])):
            return (generate_sentence_gpt2_k(sentence, tokenizer, model))

    # Compute cosine-similarits
    embeddings1 = bert_model.encode(sentence, convert_to_tensor=True)
    embeddings2 = bert_model.encode(generated_sentence, convert_to_tensor=True)
    cosine_scores = util.cos_sim(embeddings1, embeddings2)
    print("\n Cosinus score: {:.4f}".format(cosine_scores[0][0]))

    return generated_sentence.replace(sentence, "")


# Return sentence with best cos-sim-score
def get_best_out_of_three_gpt2(prompt, tokenizer, model, bert_model):
    inputs = tokenizer.encode(prompt, return_tensors='pt')

    generated_sentence_k = top_k_sampling(inputs, tokenizer, model)
    generated_sentence_p = top_p_sampling(inputs, tokenizer, model)
    generated_sentence_temp = temperature_search(inputs, tokenizer, model)

    # Compare cos-sim-score
    embeddings1 = bert_model.encode(prompt, convert_to_tensor=True)
    sentences = [generated_sentence_k, generated_sentence_p, generated_sentence_temp]
    best_score = 0
    best_sentence = ""
    for sentence in sentences:
        embeddings2 = bert_model.encode(sentence.replace(prompt, ""), convert_to_tensor=True)
        score = util.cos_sim(embeddings1, embeddings2)
        if score > best_score:
            best_score = score
            best_sentence = sentence

    print("\nGet best out of three sentences: " + best_sentence)
    # print("Cosinus score: {:.4f}".format(best_score[0][0]))
    return extract_argument_from_output(prompt, best_sentence)


# Generate 18 sentences with query embedded in prompt
def generate_a_lot_sentences_gpt2(prompt, tokenizer, model, bert_model):

    positive_1 = "What do you think? " + prompt + "\nYes because"
    positive_2 = "What do you think? " + prompt + "\nThe answer is yes"
    negative_1 = "What do you think? " + prompt + "\nNo because"
    negative_2 = "What do you think? " + prompt + "\nThe answer is no"
    neutral_1 = "What do you think? " + prompt + "\nI don't know"
    neutral_2 = "What do you think? " + prompt + "\nNot sure"

    inputs = [positive_1, positive_2, negative_1, negative_2, neutral_1, neutral_2]
    sentences = []

    for query_embedded in inputs:
        query_embedded_encoded = tokenizer.encode(query_embedded, return_tensors='pt')

        generated_sentence_k = top_k_sampling(query_embedded_encoded, tokenizer, model)
        generated_sentence_p = top_p_sampling(query_embedded_encoded, tokenizer, model)
        generated_sentence_temp = temperature_search(query_embedded_encoded, tokenizer, model)

        sentences.append(generated_sentence_k)
        sentences.append(generated_sentence_p)
        sentences.append(generated_sentence_temp)

    # Compare cos-sim-score
    prompt_tensor = bert_model.encode(prompt, convert_to_tensor=True)
    best_score = 0
    best_sentence = ""
    for sentence in sentences:
        sentence_tensor = bert_model.encode(sentence.replace(prompt, ""), convert_to_tensor=True)
        score = util.cos_sim(prompt_tensor, sentence_tensor)
        print("\n")
        print(sentence)
        print("Cosinus score: {:.4f}".format(score[0][0]))
        if score > 0.8:
            continue
        if score > best_score:
            best_score = score
            best_sentence = sentence

    print("\nGet best out of all sentences: \n" + best_sentence)
    print("Cosinus score: {:.4f}".format(best_score[0][0]))
    return best_sentence.replace(prompt, "")

def generate_average_three_sentences_gpt2(prompt, tokenizer, model, bert_model):
    inputs = tokenizer.encode(prompt, return_tensors='pt')

    generated_sentence_k = top_k_sampling(inputs, tokenizer, model)
    generated_sentence_p = top_p_sampling(inputs, tokenizer, model)
    generated_sentence_temp = temperature_search(inputs, tokenizer, model)

    print("\n Extracted k: " + extract_argument_from_output(prompt, generated_sentence_k))
    print("\n Extracted p: " + extract_argument_from_output(prompt, generated_sentence_p))
    print("\n Extracted temp: " + extract_argument_from_output(prompt, generated_sentence_temp))

    # Compute cosine-similarits
    tensor_k = bert_model.encode(generated_sentence_k, convert_to_tensor=True)
    tensor_p = bert_model.encode(generated_sentence_p, convert_to_tensor=True)
    tensor_temp = bert_model.encode(generated_sentence_temp, convert_to_tensor=True)

    # Calculate average fo tensors
    tensors = [tensor_k, tensor_p, tensor_temp]
    average_tensor = torch.stack(tensors).mean(dim=0)

    embeddings1 = bert_model.encode(prompt, convert_to_tensor=True)
    cosine_scores = util.cos_sim(embeddings1, average_tensor)
    print("\n Cosinus score: {:.4f}".format(cosine_scores[0][0]))

    return average_tensor


def extract_argument_from_output(prompt, output):
    nltk_tokenizer = nltk.tokenize.punkt.PunktSentenceTokenizer()
    tokens = nltk_tokenizer.tokenize(output.replace(prompt, ""))
    try:
        if len(word_tokenize(tokens[0])) > 7:
            return tokens[0]
        elif len(word_tokenize(tokens[0])) + len(word_tokenize(tokens[1])) > 7:
            return " ".join([token[0], token[1]])
        else:
            return " ".join([token[0], token[1], token[2]])
    finally:
        return output


if __name__ == "__main__":

    bert_model = SentenceTransformer('all-MiniLM-L6-v2')
    tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    model_gpt = GPT2LMHeadModel.from_pretrained('gpt2', pad_token_id=tokenizer.eos_token_id)
    inputs = "Climate change is real."

    generate_a_lot_sentences_gpt2(inputs, tokenizer, model_gpt, bert_model)

    # bert_model_filename = './Small_Embedding_all-MiniLM-L6-v2.pt'
    # if os.path.exists(bert_model_filename):
    #     print('Using previously trained bert model.')
    #     bert_corpus_embedding = torch.load(bert_model_filename)
    # else:
    #     print('Bert model not available.')
    #     bert_corpus_embedding = None

    # print("\nTop-K" + 100*"-")
    # print(generate_sentence_gpt2(inputs, tokenizer, model_gpt, 1))
    # print("\nTop-P" + 100*"-")
    # print(generate_sentence_gpt2(inputs, tokenizer, model_gpt, 2))
    # print("\nTemperature" + 100*"-")
    # print(generate_sentence_gpt2(inputs, tokenizer, model_gpt, 3))
    # print("\nAverage-Tensor" + 100 * "-")
    # avg_tensor = generate_average_three_sentences_gpt2(inputs, tokenizer, model_gpt, bert_model)
    # print("\nGest best out of three " + 100*"-")
    # print(get_best_out_of_three_gpt2(inputs, tokenizer, model_gpt))

    # best_5 = []
    # hits = util.semantic_search(avg_tensor, bert_corpus_embedding, 5)
    # hits = hits[0]
    #
    # # print("Top-5 Bert Retrieval hits")
    # for hit in hits[0:5]:
    #     print(hit)
