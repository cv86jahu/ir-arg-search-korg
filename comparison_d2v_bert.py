import argparse
from email.utils import formatdate
import os
from pyexpat import model
from time import sleep
from xml.dom.xmlbuilder import DocumentLS
import xml.etree.ElementTree as Et

import pandas as pd
import numpy as np
import torch
from elasticsearch import Elasticsearch
from gensim.models import Doc2Vec
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import openai
from constants import ARGS_DATASET, DOC2VEC_MODEL


from doc2vec_training import get_pandas_arguments

from constants import ELASTIC_HOST_NAME, ELASTIC_INDEX_NAME
from doc2vec_sentence_pairs import get_doc2vec_sentence_pairs
from bert import load_Model, search, preprocess_sentence
from query import perform_query
from gpt2 import generate_sentence_gpt2
from gpt3 import gpt3_text_generation

def parse_topics(topics_file):
    tree = Et.parse(topics_file)
    root = tree.getroot()
    topics = []
    for child in root:
        d = {'topic_number': int(child[0].text), 'topic_query': child[1].text}
        topics.append(d)
    topics = pd.DataFrame(topics)
    return topics


if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-dir')
    parser.add_argument('-o', '--output-dir')
    args = parser.parse_args()
    args = vars(args)

    # wait to give the containers time to start
    # sleep(60)

    # connect to elasticsearch node
    es = Elasticsearch(hosts=ELASTIC_HOST_NAME, timeout=300)
    while not es.ping():
        sleep(10)
        print('Waiting for elasticsearch node...')
        es = Elasticsearch(hosts=ELASTIC_HOST_NAME, timeout=300)

    # sanity check
    print(es.ping())
    print(es.indices.get_mapping(index=ELASTIC_INDEX_NAME))

    # handle input
    input_dir = args['input_dir']
    output_dir = args['output_dir']

    # make sure the output directory exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # parse topics
    print('Parsing topics...')
    topics = parse_topics(os.path.join(input_dir, 'topics.xml'))

    # load doc2vec model
    #d2v_model_filename = "./args_doc2vec_full_with_prepr.model"
    d2v_model_filename = DOC2VEC_MODEL
    if os.path.exists(d2v_model_filename):
        print('Using previously trained doc2vec model.')
        d2v_model = Doc2Vec.load(d2v_model_filename)
    else:
        print('Doc2vec model not available.')
        # train model?
        d2v_model = None

    # load bert model

    bertModel = load_Model()
    bert_model_filename = './Small_Embedding_all-MiniLM-L6-v2.pt'
    if os.path.exists(bert_model_filename):
        print('Using previously trained bert model.')
        bert_corpus_embedding = torch.load(bert_model_filename)
    else: 
        print('Bert model not available.')
        bert_corpus_embedding = None

    #gpt2
    tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    model_gpt = GPT2LMHeadModel.from_pretrained('gpt2', pad_token_id=tokenizer.eos_token_id)
    #gpt3

    print('Loading dataset...')
    nrows = 1000
    chunksize = 10000
    documents = []
    PREPROCESS = True

    for chunk in pd.read_csv(ARGS_DATASET, chunksize=chunksize):
        for index, doc, st in get_pandas_arguments(chunk):
            if PREPROCESS:
                if preprocess_sentence(doc) is not None:
                    documents.append(preprocess_sentence(doc))
            elif not PREPROCESS:
                documents.append(doc)
    print('finished loading dataset')

    for _, row in topics.iterrows():
        topic_number = row['topic_number']
        query = row['topic_query']

        # execute searches for doc2vec
        print(f'Now working on query{topic_number}: {query}')
        results = perform_query(query)
        formatted_results = []
        counter = 1

        for result in sorted(results['hits']['hits'], key=lambda k: k['_score']):
            # doc2vec
            sentence_pairs_doc2vec = get_doc2vec_sentence_pairs(result, d2v_model, tokenizer, model_gpt)
            print('d2v averaged score sentence pairs:')
            for s in sentence_pairs_doc2vec[0]: print(s)
            print('d2v generated score sentence pairs:')
            for s in sentence_pairs_doc2vec[1]: print(s)

            #bert (search returns top5 bert results)
            best_5 = []
            bert_results = search(result['_source']['conclusion'], corpus_embeddings=bert_corpus_embedding, top_k=100, sTransfomerModel = bertModel, documents=documents)
            
            for hit in bert_results[0:5]:
                #print("\t{:.3f}\t{}".format(hit['score'], documents[hit['corpus_id']].replace("\n", " ")))
                # generate new sentence using the matches from bert
                generated_sentence = generate_sentence_gpt2(documents[hit['corpus_id']], tokenizer, model_gpt)
                # use generated sentence to retrieve most similar sentences
                bert_results_generated = search(generated_sentence, corpus_embeddings=bert_corpus_embedding, top_k=100, sTransfomerModel = bertModel, documents=documents)
                if(documents[hit['corpus_id'] != bert_results_generated[0]['corpus_id']]):
                    best_5.append([documents[hit['corpus_id']], documents[bert_results_generated[0]['corpus_id']]])
                else:
                    best_5.append([documents[hit['corpus_id']], documents[bert_results_generated[1]['corpus_id']]])

            print('bert score sentence pairs:')
            print(best_5)

            # print('gpt2: ')
            # print(generate_sentence_gpt2(best_5[0], tokenizer, model_gpt))
        