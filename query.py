from elasticsearch import Elasticsearch

from constants import ELASTIC_HOST_NAME, ELASTIC_INDEX_NAME


def perform_query(query, weights=(1, 1, 1), es=None, index=ELASTIC_INDEX_NAME):
    if es is None:
        es = Elasticsearch(hosts=ELASTIC_HOST_NAME, http_compress=True, maxsize=10, timeout=300)

    query_body = {
        "query": {
            "multi_match": {
                "query": query,
                "fields": [f"conclusion^{weights[0]}",
                           f"premises.text^{weights[1]}",
                           f"sentences.sent_text^{weights[2]}"]
            }
        }
    }
    response = es.search(index=index, body=query_body, size=1000)
    return response


if __name__ == "__main__":
    es = Elasticsearch(hosts=ELASTIC_HOST_NAME, http_compress=True, maxsize=10, timeout=300)
    print(es.indices.get_mapping(index=ELASTIC_INDEX_NAME))
    print(es.cat.count(index=ELASTIC_INDEX_NAME, params={"format": "json"}))

    query = "home schooling"
    result = perform_query(query, (1,1,1), es)


    all_hits = result['hits']['hits']

    # iterate the nested dictionaries inside the ["hits"]["hits"] list
    for num, doc in enumerate(all_hits):

        for key, value in doc.items():
            print(key, "-->", value)

        # print a few spaces between each doc for readability
        print("\n\n")

    print("total hits:", len(result["hits"]["hits"]))
