import openai
from constants import OPENAI_API_KEY

# Load your API key from an environment variable or secret management service
openai.api_key = OPENAI_API_KEY

# argument = "Public school is better for developing life skills than Home Schooling."
# argument = "We should promote sex education in schools."

def gpt3_text_generation(sentence):
    response = openai.Completion.create(engine="davinci", prompt=sentence, max_tokens=50)
    return response.choices[0].text
