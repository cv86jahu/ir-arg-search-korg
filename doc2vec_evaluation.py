from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import pandas as pd

from doc2vec_training import get_pandas_arguments
from constants import ARGS_DATASET, DOC2VEC_MODEL


if __name__ == '__main__':
    filename = DOC2VEC_MODEL
    filename_preproc = "./args_doc2vec_with_prepr.model"

    print('Using previously trained model.')
    model = Doc2Vec.load(filename)

    model_preproc = Doc2Vec.load(filename_preproc)

    print('Loading argument dataset...')
    args_1k = pd.read_csv(ARGS_DATASET, nrows=1000)

    # import unseen sentencesdsad
    test_sentences = pd.read_csv(ARGS_DATASET, nrows=2000)[1000:1001]

    # evaluate how the vectors look like for test_sentences
    for doc_id, doc, st in get_pandas_arguments(test_sentences):
        inferred_vec = model.infer_vector(doc.split(), epochs=10)
        print('------------ Comparing sentence:')
        print(doc_id, doc)
        print('------------')
        for sim_sentence, sim in model.dv.most_similar([inferred_vec], topn=5):
            # get argument id and then look for sentence in the respective argument
            argument_id = sim_sentence.split("__")[0]
            argument = args_1k.loc[args_1k['id'] == argument_id]
            for sentence_id, sentence, stance in get_pandas_arguments(argument):
                if sentence_id == sim_sentence:
                    print(sentence_id, sentence, stance, sim)

            # for index, sentence, stance in get_pandas_arguments(args_1k):
            #     if index == sim_sentence:
            #         print(sim_sentence, sentence, sim, stance)

    # next step: do similiarity check of vectors for sentences from top n results
    index_temp = './index'

    # example sentence
    example_sentence = 'S4ff8fa3e-A7b080565__PREMISE__3'
    sims_example_sentence = model.dv.most_similar(example_sentence, topn = 10)

    # for index, sentence, stance in get_pandas_arguments(args_1k):
    #     if index == example_sentence:
    #         print('Asked sentence:')
    #         print(index, sentence, stance)
    #         print('_____________')
    #         for sim_sentence in sims_example_sentence:
    #             for i, s, st in get_pandas_arguments(args_1k):
    #                 if i == sim_sentence[0]:
    #                     print(i, s, sim_sentence[1], st)

    #index = similarities.Similarity(index_temp, model, num_features=10)
    # index2 = similarities.MatrixSimilarity(model, num_features=10)
    #
    # query = 'bush'
    # print(index2[query])