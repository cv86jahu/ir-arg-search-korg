import random
from elasticsearch import Elasticsearch
import pandas as pd
from skopt import gp_minimize, dump, load
from sklearn.metrics import ndcg_score
from gensim.models import Doc2Vec
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import torch
from sentence_transformers import SentenceTransformer
import ast
import pickle
import os
from collections import defaultdict
import math
from pprint import pprint

from query import perform_query
from run import parse_topics
from constants import ELASTIC_HOST_NAME, ELASTIC_BASELINE_INDEX_NAME, ELASTIC_INDEX_NAME, ARGS_DATASET
from doc2vec_sentence_pairs import get_doc2vec_sentence_pairs
from doc2vec_training import get_pandas_arguments
from bert import search, preprocess_sentence
from gpt2 import generate_sentence_gpt2


def get_base_evaluation_data(weight_vec=(2.1469393229524596, 1.1211051817895195, 1.5540994792133285), index_name=ELASTIC_INDEX_NAME) -> dict:
    es = Elasticsearch(hosts=ELASTIC_HOST_NAME, http_compress=True, maxsize=10, timeout=300)
    topics = parse_topics('topics.xml')
    results_all_topics = {}
    for task_num in range(51, 61):
        topic_str = topics[topics['topic_number'] == task_num].iloc[0].topic_query
        results = perform_query(topic_str[:-1], weight_vec, es, index=index_name)
        results = sorted(results['hits']['hits'], key=lambda k: k['_score'])
        results_all_topics[(task_num, topic_str)] = results

    return results_all_topics

def evaluate_base_retrieval(weight_vec=(1, 1, 1)):
    print('weight vec:', weight_vec)
    results_all_topics = get_base_evaluation_data(weight_vec, ELASTIC_INDEX_NAME)
    quality_judge = pd.read_csv('touche-task1-51-100-relevance.qrels', sep=' ', header=None)
    ndcg_values = []
    for task_num, results in results_all_topics.items():
        scores_list = [[]]
        rel_judgement_list = [[]]

        for pos, result in enumerate(results):
            pos += 1
            scores_list[0].append(result['_score'])
            quality_entries_res = quality_judge[(quality_judge[2] == result['_id']) & (quality_judge[0] == task_num[0])]
            if not quality_entries_res.empty:
                #print(quality_entries_res.iloc[0][3])
                quality_rank = quality_entries_res.iloc[0][3]
                if quality_rank < 0:
                    rel_judgement_list[0].append(0)
                else:
                    rel_judgement_list[0].append(quality_rank)

            else:
                rel_judgement_list[0].append(0)

        ndcg = ndcg_score(rel_judgement_list, scores_list, k=5)
        ndcg_values.append(ndcg)
        #print(rel_judgement_list)
        #print(scores_list)
        #print(f'ndcq: {ndcg}')

    print('Mean ndcc:', sum(ndcg_values)/len(ndcg_values))
    return 1 - sum(ndcg_values)/len(ndcg_values)


def evaluate_sentence_pair_retrieval(filename):
    qual_results = defaultdict(list)
    with open(filename, 'r') as qual_file:
        for line in qual_file.readlines():
            line = line.split(' ')
            topic_num = int(line[0])
            grade = int(line[3])
            if grade < 0:
                qual_results[topic_num].append(0)
            else:
                qual_results[topic_num].append(grade)

    ndcg_values = []
    ndcg_5_values = []
    for topic_num, grades in qual_results.items():
        scores_list = [list(range(len(grades),0,-1))]
        rel_judgement_list = [grades]

        ndcg = ndcg_score(rel_judgement_list, scores_list)
        ndcg_5 = ndcg_score(rel_judgement_list, scores_list, k=5)
        ndcg_values.append(ndcg)
        ndcg_5_values.append(ndcg_5)
        print(f'Topic {topic_num}: NDCG: {ndcg} NDCG@5 {ndcg_5}')

    print('Mean ndcg:', sum(ndcg_values) / len(ndcg_values))
    print('Mean ndcg@5:', sum(ndcg_5_values) / len(ndcg_5_values))


def baseline_sentence_retrieval():
    results_all_topics = get_base_evaluation_data()

    with open('rnd_sentence_pairs', 'w') as rnd_sentences_file:
        with open('rnd_sentence_quality', 'w') as rnd_sentences_qual_file:
            for topic_num, results in results_all_topics.items():
                sentences = []
                for result in results:
                    for sentence in result['_source']['sentences']:
                        sentences.append(sentence)

                rnd_sentence_pairs = []
                random.seed(7865730586421)
                random.shuffle(sentences)
                for i in range(0, 20, 2):
                    rnd_sentence_pairs.append((sentences[i], sentences[i+1]))

                rnd_sentences_file.write(f'Topic number {topic_num}\n')
                for pair in rnd_sentence_pairs:
                    rnd_sentences_file.write(f'\t{pair[0]["sent_text"]} ({pair[0]["sent_id"]})\n')
                    rnd_sentences_file.write(f'\t{pair[1]["sent_text"]} ({pair[1]["sent_id"]})\n\n')

                    rnd_sentences_qual_file.write(f'{topic_num[0]} 0 {pair[0]["sent_id"]},{pair[1]["sent_id"]}\n')

def doc2vec_sentence_retrieval():
    print('get base results')
    results_all_topics = get_base_evaluation_data()
    model_filename = "./args_doc2vec_full_with_prepr.model"
    print('load model')
    model = Doc2Vec.load(model_filename)

    print('get tokenizer')
    tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    print('get gpt2 model')
    model_gpt = GPT2LMHeadModel.from_pretrained('gpt2', pad_token_id=tokenizer.eos_token_id)
    with open('doc2vec_gen__sentence_pairs', 'w') as doc2vec_gen_sentences_file:
        with open('doc2vec_gen_sentence_quality', 'w') as doc2vec_gen_sentences_qual_file:
            with open('doc2vec_avg__sentence_pairs', 'w') as doc2vec_avg_sentences_file:
                with open('doc2vec_avg_sentence_quality', 'w') as doc2vec_avg_sentences_qual_file:
                    for topic_num, results in results_all_topics.items():
                        print(f'Topic: {topic_num}')
                        doc2vec_gen_sentences_file.write(f'Topic number {topic_num}\n')
                        doc2vec_avg_sentences_file.write(f'Topic number {topic_num}\n')
                        for result in results[:10]:
                            sentence_pairs_doc2vec = get_doc2vec_sentence_pairs(result, model, tokenizer, model_gpt)
                            # pprint(sentence_pairs_doc2vec[0])
                            for pair in sentence_pairs_doc2vec[0][:1]:
                                doc2vec_gen_sentences_file.write(f'\t{pair[2]} ({pair[0]})\n')
                                doc2vec_gen_sentences_file.write(f'\t{pair[3]} ({pair[1]})\n\n')

                                doc2vec_gen_sentences_qual_file.write(f'{topic_num[0]} 0 {pair[0]},{pair[1]}\n')
                            for pair in sentence_pairs_doc2vec[1][:1]:
                                doc2vec_avg_sentences_file.write(f'\t{pair[2]} ({pair[0]})\n')
                                doc2vec_avg_sentences_file.write(f'\t{pair[3]} ({pair[1]})\n\n')

                                doc2vec_avg_sentences_qual_file.write(f'{topic_num[0]} 0 {pair[0]},{pair[1]}\n')


def bert_sentence_retrieval():
    print('get base results')
    results_all_topics = get_base_evaluation_data()

    print('Loading bert model...')
    model_name = 'all-MiniLM-L12-v2'
    sTransfomerModel = SentenceTransformer(model_name)
    sTransfomerModel.max_seq_length = 256

    print('loading sbert embeddings...')
    bert_embeddings_filename = 'Big_Embedding_all-MiniLM-L12-v2.pt'
    corpus_embedding = torch.load(bert_embeddings_filename)

    print('loading documents...')
    doc_filename = 'documents.pkl'
    if os.path.exists(doc_filename):
        with open(doc_filename, 'rb') as pickle_file:
            documents = pickle.load(pickle_file)
    else:
        chunksize = 25000
        documents = []
        PREPROCESS = True

        args = pd.read_csv(ARGS_DATASET, chunksize=chunksize, sep=',',
                                 converters={'premises': ast.literal_eval, 'sentences': ast.literal_eval})
        for index, doc, st in get_pandas_arguments(args):
            if PREPROCESS:
                if preprocess_sentence(doc) is not None:
                    documents.append((index, preprocess_sentence(doc)))
            elif not PREPROCESS:
                documents.append((index, doc))
        with open(doc_filename, 'wb') as pickle_file:
            pickle.dump(documents, pickle_file)

    print('get tokenizer')
    tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    print('get gpt2 model')
    model_gpt = GPT2LMHeadModel.from_pretrained('gpt2', pad_token_id=tokenizer.eos_token_id)
    with open('bert_sentence_pairs', 'w') as bert_sentences_file:
        with open('bert_sentence_quality', 'w') as bert_sentences_qual_file:
            for topic_num, results in results_all_topics.items():
                print(f'Topic: {topic_num}')
                bert_sentences_file.write(f'Topic number {topic_num}\n')
                for result in results[:2]:
                    top_k = 100
                    best_5 = []
                    bert_results = search(result['_source']['conclusion'], corpus_embeddings=corpus_embedding, top_k=top_k,
                                          sTransfomerModel=sTransfomerModel, documents=documents)

                    for hit in bert_results[0:5]:
                        generated_sentence = generate_sentence_gpt2(documents[hit['corpus_id']][1], tokenizer, model_gpt)
                        bert_results_generated = search(generated_sentence, corpus_embeddings=corpus_embedding, top_k=top_k,
                                                        sTransfomerModel=sTransfomerModel, documents=documents)

                        if (documents[hit['corpus_id'] != bert_results_generated[0]['corpus_id']]):
                            best_5.append([documents[hit['corpus_id']], documents[bert_results_generated[0]['corpus_id']]])
                        else:
                            best_5.append([documents[hit['corpus_id']], documents[bert_results_generated[1]['corpus_id']]])

                    print(best_5)
                    for pair in best_5:
                        bert_sentences_file.write(f'\t{pair[0][1]} ({pair[0][0]})\n')
                        bert_sentences_file.write(f'\t{pair[1][1]} ({pair[1][0]})\n\n')

                        bert_sentences_qual_file.write(f'{topic_num[0]} 0 {pair[0][0]},{pair[1][0]}\n')



def optimize_search_weights():
    res = gp_minimize(evaluate_base_retrieval, [(0.0, 3.0), (0.0, 3.0), (0.0, 3.0)])
    print(res)
    dump(res, 'opt_res.pkl')


if __name__ == "__main__":
    #optimize_search_weights()
    #evaluate_base_retrieval((2.1469393229524596, 1.1211051817895195, 1.5540994792133285))
    #baseline_sentence_retrieval()
    #doc2vec_sentence_retrieval()
    #bert_sentence_retrieval()
    evaluate_sentence_pair_retrieval('evaluation/bert_sentence_quality_graded')
    #evaluate_base_retrieval((2.9232907023974777, 1.5060663918776398, 1.041082522090874))
