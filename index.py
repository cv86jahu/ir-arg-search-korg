import pandas as pd
from pandas.io.parsers import TextFileReader
from ast import literal_eval
from elasticsearch import Elasticsearch, helpers

from constants import ELASTIC_HOST_NAME, ELASTIC_INDEX_NAME, ELASTIC_BASELINE_INDEX_NAME, ARGS_DATASET


def document_generator_from_dataframe(df, index, fields_to_index):
    counter = 1
    for chunk in df:
        print(f'Processing {counter}. chunk...')
        counter += 1
        for _, row in chunk.iterrows():
            row_as_dict = row.replace('', 'empty').to_dict()
            yield {
                "_index": index,
                "_id": row['id'],
                "_source": {k: row_as_dict[k] for k in fields_to_index}
            }


def create_index(index_name, body, dataset):
    if es.indices.exists(index=index_name):
        es.indices.delete(index=index_name)
    es.indices.create(index=index_name, body=body)
    gen = document_generator_from_dataframe(dataset, index_name, ['id', 'conclusion', 'premises', 'sentences'])
    print(f"Now indexing on {index_name}...")
    helpers.bulk(es, gen)
    print("Done indexing.")


if __name__ == "__main__":
    es = Elasticsearch(hosts=ELASTIC_HOST_NAME, http_compress=True, maxsize=150, timeout=300)

    print('Building dataframe...')
    dataset = pd.read_csv(ARGS_DATASET, sep=',', chunksize=50000,
                          converters={'premises': literal_eval, 'sentences': literal_eval})

    print('Done bulding.')

    body = {
        'settings': {
            'similarity': {
                'my_dirichlet': {
                    'type': 'LMDirichlet',
                  #  "mu": "2000" #2000 is default value
                }
            },
            'analysis': {
              "analyzer": {
                "korg_analyzer": {
                  "tokenizer": "standard",
                  "filter": [
                    "asciifolding",             # Converts alphabetic, numeric, and symbolic characters that are not in the Basic Latin Unicode block (first 127 ASCII characters) to their ASCII equivalent, if one exists 
                    "lowercase",                 
                    "kstem",                    # Use Krovetz Stemming
                    "wordnet_syns",
                    "custom_stopwords_filter"   # Use our own stopwords list
                  ]
                }
              },
              "filter": {
                "custom_stopwords_filter": {
                  "type": "stop",
                  "stopwords_path": "resources/stopwords_en.txt"
                },
                "wordnet_syns" : {
                  "type" : "synonym",
                  "format" : "wordnet",
                  "synonyms_path" : "resources/wn_s.pl"
                }
              }
           }
        },
        'mappings': {
            'properties': {
                'id': {'type': 'keyword'},
                'conclusion': {'type': 'text', 'similarity': 'my_dirichlet', "analyzer": "korg_analyzer"},
                'premises': {'properties': {
                    'text': {'type': 'text', 'similarity': 'my_dirichlet', "analyzer": "korg_analyzer"},
                    'stance': {'type': 'keyword'}
                }},
                'sentences': {'properties': {
                    'sent_id': {'type': 'keyword'},
                    'sent_text': {'type': 'text', 'similarity': 'my_dirichlet', "analyzer": "korg_analyzer"}
                }}
            }
        }
    }
    body_without_query_expansion = {
        'settings': {
            'similarity': {
                'my_dirichlet': {'type': 'LMDirichlet'}
            }
        },
        'mappings': {
            'properties': {
                'id': {'type': 'keyword'},
                'conclusion': {'type': 'text', 'similarity': 'my_dirichlet'},
                'premises': {'properties': {
                    'text': {'type': 'text', 'similarity': 'my_dirichlet'},
                    'stance': {'type': 'keyword'}
                }},
                'sentences': {'properties': {
                    'sent_id': {'type': 'keyword'},
                    'sent_text': {'type': 'text', 'similarity': 'my_dirichlet'}
                }}
            }
        }
    }
    create_index(ELASTIC_INDEX_NAME, body, dataset)
    # create_index(ELASTIC_BASELINE_INDEX_NAME, body_without_query_expansion, dataset)
